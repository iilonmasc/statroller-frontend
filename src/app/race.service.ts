import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { RACES } from './mock-races';
import { Race } from './race';

@Injectable({
  providedIn: 'root'
})
export class RaceService {

  currentRace: Race;

  constructor() { }

  getRaces(): Observable<Race[]> {
    return of(RACES)
  }
  setRace(race: Race): void {
    this.currentRace = race;
  }
}
