import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-color-scheme',
  templateUrl: './color-scheme.component.html',
  styleUrls: ['./color-scheme.component.scss']
})
export class ColorSchemeComponent implements OnInit {

  colors: string[] = ['primary', 'secondary', 'tertiary']
  lightings: string[] = ['dark-strong', 'dark-light', 'regular', 'light-light', 'light-strong']

  constructor() { }

  ngOnInit() {
  }

}
