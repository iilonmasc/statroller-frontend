import { Injectable } from '@angular/core';
import { Stat, StatField } from './stat';
import { Observable, of } from 'rxjs';
import { map, finalize } from 'rxjs/operators';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../environments/environment';
import { Bonus } from './bonus';

@Injectable({
  providedIn: 'root'
})
export class StatService {

  statFields: StatField[] = [
    { name: 'str', index: undefined, stat: undefined },
    { name: 'dex', index: undefined, stat: undefined },
    { name: 'con', index: undefined, stat: undefined },
    { name: 'int', index: undefined, stat: undefined },
    { name: 'wis', index: undefined, stat: undefined },
    { name: 'cha', index: undefined, stat: undefined }
  ];
  currentStats: Stat[];
  availableStats: boolean[] = [true, true, true, true, true, true];
  anyBonus: number;
  anyBonusCurrent: number;
  additions: Bonus[] = [];


  constructor(private http: HttpClient) { }

  getAnyBonus(): number {
    return this.anyBonus;
  }
  getAnyBonusCurrent(): number {
    return this.anyBonusCurrent;
  }
  getStatFields(): StatField[] {
    return this.statFields;
  }
  getAvailableStats(): boolean[] {
    return this.availableStats;
  }

  getStats(): Observable<Stat[]> {
    this.availableStats = [true, true, true, true, true, true];
    return this.http.get<Stat[]>(environment.baseURL)
      .pipe(
        map(stats => stats['stats']),
        map(stats => this.currentStats = stats),
      );
  }
  registerAddition(stat: string): void {
    this.additions.push({ stat: stat, value: 1 })
        this.anyBonusCurrent -= 1
  }
  removeAddition(stat: string): void {
    for (let index in this.additions) {
      let addition = this.additions[index]
      if (addition.stat == stat) {
        this.additions.splice(+index, 1)
        this.anyBonusCurrent += 1
        break;
      }
    }
  }

  getAdditions(): Bonus[]{
    return this.additions;
  }

}
