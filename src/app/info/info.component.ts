import { Component, Input, OnInit } from '@angular/core';
import { Race } from '../race';
import { Stat } from '../stat';
import { RaceService } from '../race.service';
import { StatService } from '../stat.service';

@Component({
  selector: 'app-info',
  templateUrl: './info.component.html',
  styleUrls: ['./info.component.scss']
})
export class InfoComponent implements OnInit {

  @Input() appTitle: string;
  @Input() appVersion: string;
  @Input() sidenav: any;

  constructor(private raceService: RaceService, private statService: StatService) { }

  ngOnInit() {
  }
  getRace(): Race {
    return this.raceService.currentRace;
  }
  getStats(): Stat[] {
    return this.statService.currentStats;
  }

}
