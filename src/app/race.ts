import { Bonus } from './bonus';
export interface Race {
    name: string;
    bonus: Bonus[];
}