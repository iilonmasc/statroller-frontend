import { Component, OnInit } from '@angular/core';
import { Bonus } from '../bonus';
import { Race } from '../race';
import { RaceService } from '../race.service';
import { Stat, StatField } from '../stat';
import { StatService } from '../stat.service';


@Component({
  selector: 'app-assign',
  templateUrl: './assign.component.html',
  styleUrls: ['./assign.component.scss']
})

export class AssignComponent implements OnInit {

  statFields: StatField[];
  availableStats: boolean[];
  anyBonus: number;
  anyBonusCurrent: number;
  additions: Bonus[];

  constructor(private raceService: RaceService, private statService: StatService) { }

  ngOnInit(): void {
    this.getAvailableStats();
    this.getStatFields();
    this.getAnyBonus();
  }
  getAnyBonus(): number {
    this.anyBonus = this.statService.getAnyBonus();
    return this.anyBonus;
  }
  getAnyBonusCurrent(): number {
    this.anyBonusCurrent = this.statService.getAnyBonusCurrent();
    return this.anyBonusCurrent;
  }
  getRace(): Race {
    return this.raceService.currentRace;
  }
  getStatFields(): StatField[] {
    this.statFields = this.statService.getStatFields();
    return this.statFields;
  }
  getAvailableStats(): boolean[] {
    this.availableStats = this.statService.getAvailableStats();
    return this.availableStats;
  }
  getAdditions(): Bonus[] {
    this.additions = this.statService.getAdditions();
    return this.additions;
  }
  getStats(): Object[] {
    return this.statService.currentStats ? this.statService.currentStats : undefined;
  }
  getSingleStat(stat: string): Stat {
    let statObject: StatField = undefined;
    for (let statfield of this.statFields) {
      statObject = statfield.name == stat ? statfield : undefined;
      if (statObject) { break; }
    }
    if (statObject !== undefined && statObject.stat) {
      let returnStat = { value: statObject.stat.value, modifier: statObject.stat.modifier };
      let race = this.getRace();
      if (race) {
        for (let bonus of race.bonus) {
          if (bonus.stat == stat || bonus.stat == 'all') {
            returnStat.value = +returnStat.value + +bonus.value;
            let modifier = Math.floor((+returnStat.value - 10) / 2);
            returnStat.modifier = (modifier <= 0 ? "" : "+") + modifier;
          }
        }
      }
      this.getAdditions()
      if (this.additions) {
        for (let bonus of this.getAdditions()) {
          if (bonus.stat == stat) {
            returnStat.value = +returnStat.value + +bonus.value;
            let modifier = Math.floor((+returnStat.value - 10) / 2);
            returnStat.modifier = (modifier <= 0 ? "" : "+") + modifier;
          }
        }
      }
      return returnStat
    }
    return undefined;
  }
  getBonusSize(stat: string): number {
    let race = this.getRace()
    let additions = this.getAdditions()
    let totalBonus = 0;
    if (race) {
      for (let bonus of race.bonus) {
        if (bonus.stat == stat) {
          totalBonus += +bonus.value
        }
      }
    }
    if (additions) {
      for (let bonus of additions) {
        if (bonus.stat == stat) {
          totalBonus += +bonus.value
        }
      }
    }
    return totalBonus

  }
  statToggle(stat: string, index: number): void {
    let statObject: StatField = undefined;
    for (let statfield of this.statFields) {
      statObject = statfield.name == stat ? statfield : undefined;
      if (statObject) { break; }
    }
    if (statObject !== undefined) {
      if (statObject.index === undefined) { // this stat/index combination was never chosen before
        statObject.index = index;
        statObject.stat = this.statService.currentStats[index];
        this.availableStats[statObject.index] = !this.availableStats[statObject.index]; // toggle all statfields with the same index
      } else if (statObject.index !== index) { // this stat is the same, index is another
        this.availableStats[statObject.index] = true; // free up old index
        this.availableStats[index] = false; // close new index
        statObject.index = index; // set current stat/index combination to new field
        statObject.stat = this.statService.currentStats[index];
      } else {
        this.availableStats[index] = !this.availableStats[index]; // toggle new index
        statObject.stat = undefined;
        statObject.index = undefined; // set current stat/index combination to new field
      }
      this.statService.availableStats = this.availableStats;
    }
  }
  isBonusField(stat: string): boolean {
    let race = this.getRace();
    if (race) {
      for (let bonus of race.bonus) {
        if (bonus.stat == stat || bonus.stat == 'all') {
          return true
        }
      }
      this.getAdditions()
      if (this.additions) {
        for (let bonus of this.getAdditions()) {
          if (bonus.stat == stat) {
            return true
          }
        }
      }
    }
    return false
  }

  isStatEnabled(stat: string): boolean {
    let race = this.getRace();
    if (race) {
      for (let bonus of race.bonus) {
        if (bonus.stat == stat) {
          return false
        }
      }
      return true
    }
  }

  isDisabled(stat: string, index: number): boolean {
    let statObject: StatField = undefined;
    for (let statfield of this.statFields) {
      statObject = (statfield.name == stat) ? statfield : undefined;
      if (statObject) { break; }
    }
    if (statObject.index == index) {
      return false
    }
    return !this.availableStats[index]
  }
  isPressed(stat: string, index: number): boolean {
    let statObject: StatField = undefined
    for (let statfield of this.statFields) {
      statObject = (statfield.name == stat) ? statfield : undefined;
      if (statObject) { break; }
    }
    if (statObject !== undefined) {

      if (statObject.index === index && !this.availableStats[index]) { // stat's set index is the same as current button and this index is not free aka. this is the button currently pressed
        return true;
      }
      return false;
    }
    return false;

  }
  resetChoices(): void {
    console.log("DOING IT")
    this.statService.statFields = [
      { name: 'str', index: undefined, stat: undefined },
      { name: 'dex', index: undefined, stat: undefined },
      { name: 'con', index: undefined, stat: undefined },
      { name: 'int', index: undefined, stat: undefined },
      { name: 'wis', index: undefined, stat: undefined },
      { name: 'cha', index: undefined, stat: undefined }
    ];
    this.statService.availableStats = [true, true, true, true, true, true];
    this.getAvailableStats();
    this.getStatFields()

  }
  increase(stat: string): void {
    if (this.statService.getAnyBonusCurrent() > 0) {
      this.statService.registerAddition(stat)
      console.log("increasing")
    } else {
      console.log("no points left")
    }
  }
  decrease(stat: string): void {
    if (this.statService.getAnyBonusCurrent() !== this.statService.getAnyBonus()) {
      this.statService.removeAddition(stat)
      console.log("decreasing")
    } else {
      console.log("no points left")
    }
  }
  canIncrease(): boolean {
    return (this.getAnyBonusCurrent() > 0) ? true : false
  }
  canDecrease(stat: string): boolean {
    for (let addition of this.getAdditions()) {
      if (addition.stat == stat) {
        return ((this.getAnyBonus() - this.getAnyBonusCurrent()) > 0) ? true : false
      }
    }
    return false;
  }

}
