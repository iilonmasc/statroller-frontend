import { Component, OnInit, Input } from '@angular/core';
import { Stat } from '../stat';

@Component({
  selector: 'app-points',
  templateUrl: './points.component.html',
  styleUrls: ['./points.component.scss']
})
export class PointsComponent implements OnInit {

  @Input() stats: Stat[];
  @Input() loading: boolean;

  constructor() { }

  ngOnInit() {
  }

  getPercentage(): number {
    let minimum = 18;
    let maxium = 108;
    let current = this.getStatTotal();
    return (current - minimum) / (maxium - minimum) * 100;
  }

  getStatTotal(): number {
    if (this.stats){
    let total = 0;
    for (let stat of this.stats) {
      total = total + +stat.value;
    }
    return total;
  }
  return 0;
  }

}
