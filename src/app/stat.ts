export interface Stat {
    value: number;
    modifier: string;
}
export interface StatField {
  name: string;
  index: number;
  stat: Stat;
}