import { MediaMatcher } from '@angular/cdk/layout';
import { ChangeDetectorRef, Component } from '@angular/core';
import { isDevMode } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'D&D 5e character roller';
  version = 'v0.8.2';
  links = [
    { path: '/stats', text: 'Roll a stat array' },
    { path: '/races', text: 'Pick a Race' },
    { path: '/assign', text: 'Assign values to stats', disabled: true },
  ];
  mobileQuery: MediaQueryList;
  opened: boolean;
  getDevMode(): boolean {
    return isDevMode();
  }
  private _mobileQueryListener: () => void;

  constructor(changeDetectorRef: ChangeDetectorRef, media: MediaMatcher) {
    this.mobileQuery = media.matchMedia('(max-width: 600px)');
    this._mobileQueryListener = () => changeDetectorRef.detectChanges();
    this.mobileQuery.addListener(this._mobileQueryListener);
  }

  ngOnDestroy(): void {
    this.mobileQuery.removeListener(this._mobileQueryListener);
  }
}
