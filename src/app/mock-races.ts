import { Race } from './race';

export const RACES: Race[] = [
    { name: 'dragonborn', 'bonus': [{ 'stat': 'str', 'value': 2 }, { 'stat': 'cha', 'value': 1 }] },
    { name: 'elf', 'bonus': [{ 'stat': 'dex', 'value': 2 }, { 'stat': 'int', 'value': 1 }] },
    { name: 'half elf', 'bonus': [{ 'stat': 'cha', 'value': 2 }, { 'stat': 'any', 'value': 2 }] },
    { name: 'hill dwarf', 'bonus': [{ 'stat': 'con', 'value': 2 }, { 'stat': 'wis', 'value': 1 }] },
    { name: 'human', 'bonus': [{ 'stat': 'all', 'value': 1 }] }
]