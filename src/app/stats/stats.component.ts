import { Component, OnInit } from '@angular/core';
import { finalize } from 'rxjs/operators';
import { Stat } from '../stat';
import { StatService } from '../stat.service';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-stats',
  templateUrl: './stats.component.html',
  styleUrls: ['./stats.component.scss']
})
export class StatsComponent implements OnInit {

  stats: Stat[]
  loading: boolean;

  constructor(private statService: StatService, private _snackBar: MatSnackBar) { }

  getStats(): void {
    this.loading = true;
    this.statService.getStats().pipe(finalize(() => { this.loading = false; this.openSnackBar('Done fetching Data', 'Dismiss') })).subscribe(stats => this.stats = stats);
  }

  ngOnInit() {
    if (this.statService.currentStats == undefined) {
      this.getStats();
    } else {
      this.stats = this.statService.currentStats;
    }
  }
  openSnackBar(message: string, action: string) {
    this._snackBar.open(message, action, {
      duration: 2000,
    });

  }


}
