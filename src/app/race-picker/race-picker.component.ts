import { Component, OnInit } from '@angular/core';
import { RaceService } from '../race.service';
import { finalize } from 'rxjs/operators';
import { Race } from '../race';
import { StatService } from '../stat.service';

@Component({
  selector: 'app-race-picker',
  templateUrl: './race-picker.component.html',
  styleUrls: ['./race-picker.component.scss']
})
export class RacePickerComponent implements OnInit {

  loading: boolean = true;
  races: Race[];
  selectedRace: Race;

  constructor(private statService: StatService, private raceService: RaceService) { }

  ngOnInit() {
    this.getRaces();
  }
  getRaces(): void {
    this.loading = true;
    this.raceService.getRaces().pipe(finalize(() => this.loading = false)).subscribe(races => this.races = races);
  }
  onSelect(race: Race): void {
    this.selectedRace = race;
    this.raceService.setRace(race);
    let bonusFound = false
    for(let bonus of race.bonus){
      if (bonus.stat == 'any'){
        this.statService.additions = [];
        this.statService.anyBonus = bonus.value;
        this.statService.anyBonusCurrent = bonus.value;
        bonusFound = true
      }
  }
  if(!bonusFound){
        this.statService.anyBonus = 0;
        this.statService.anyBonusCurrent = 0;
        this.statService.additions = [];
  }
  }

}
