export interface Bonus {
    stat: string;
    value: number;
}