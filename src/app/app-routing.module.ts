import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { StatsComponent } from './stats/stats.component';
import { RacePickerComponent } from './race-picker/race-picker.component';
import { AssignComponent } from './assign/assign.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';

const routes: Routes = [
  { path: 'stats', component: StatsComponent },
  { path: 'races', component: RacePickerComponent },
  { path: 'assign', component: AssignComponent },
  { path: '', redirectTo: '/assign', pathMatch: 'full' }, // redirect to `first-component`
  { path: '**', component: PageNotFoundComponent },  // Wildcard route for a 404 page
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
